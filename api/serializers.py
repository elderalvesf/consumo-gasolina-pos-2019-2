from rest_framework import serializers

from api import models


class SupplySerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Supply
        fields = '__all__'
